// supress stylistic warnings
#![allow(unused_parens)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(unused)]
//#![allow()]

use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize)]
pub struct Config {
	
	pub channels: Vec <channel>,
	pub servers: Vec <String>,
	pub folder: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct channel {
	
	pub name: String,
	pub id: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Response {
	
	pub videos: Vec<Video>,
	pub continuation: Option<String>, // doesn't exist for small channels
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Video {
	
	pub r#type: String,
	pub title: String,
	pub videoId: String,
	pub author: String,
	pub authorId: String,
	pub authorUrl: String,
	pub authorVerified: bool,
	pub videoThumbnails: Vec<Thumbnails>,
	pub description: String,
	pub descriptionHtml: String,
	pub viewCount: u64, // most-viewed video has 13B views, too big for u32
	pub viewCountText: String,
	pub published: u64,
	pub publishedText: String,
	pub lengthSeconds: u16, // max video length is 12 h, u16 gives 18 h
	pub liveNow: bool,
	pub premium: bool,
	pub isUpcoming: bool,
	pub premiereTimestamp: Option<u64>, // doesn't exist in non-premier videos
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Thumbnails {
	
	pub quality: String,
	pub url: String,
	pub width: u16,
	pub height: u16,
}

pub struct Stream {
	
	pub author: String,
	pub title: String,
	pub timestamp: u64,
	pub link: String,
}

pub struct Table {
	
	pub entries: Vec <Table_Entry>,
	pub date_length: usize,
	pub author_length: usize,
	pub title_length: usize,
}

pub struct Table_Entry {
	
	pub date: String,
	pub author: String,
	pub title: String,
	pub link: String,
	pub new_segment: bool,
}
