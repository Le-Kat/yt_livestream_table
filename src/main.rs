// supress stylistic warnings
#![allow(unused_parens)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
//#![allow()]

mod Data;
use crate::Data::{Config, Response, Stream, Table, Table_Entry};

use std::io::{Write, Error as IOError, Read};
use std::thread::{self, JoinHandle};
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use std::fs::{self, ReadDir, DirEntry};
use std::fs::File;
use std::path::Path;

use anyhow::Error;
use clap::Parser;
use reqwest::blocking as reqwest;
use chrono::{DateTime, Local};

#[derive (Parser, Debug)]
#[command (author, version, about, long_about = None)]
struct Args {
	
	#[arg (short, long, default_value = "config.toml")]
	config: String,
	
	#[arg (long)]
	sync: bool,
	
	#[arg (short, long)]
	quiet: bool,
}

fn main() {
	
	let args: Args = Args::parse();
	
	// read config file
	let config: Config;
	let filePath: &Path = Path::new (& args.config);
	let mut file: File;
	let mut fileContents: String = Default::default();
	
	if (!filePath.is_file()) {
		
		panic! ("config file missing!");
	}
	
	file = match File::open (filePath) {
		Ok (ok) => { ok },
		Err (_) => { panic! ("unable to read config file") },
	};
	
	let _ = file.read_to_string (&mut fileContents);
	config = match toml::from_str (&fileContents) {
		Ok (ok) => { ok },
		Err (_) => { panic! ("unable to parse config file") },
	};
	
	// update JSON
	if (args.sync) {
		
		let mut handlers: Vec <JoinHandle <()>> = Vec::with_capacity (config.channels.len());
		
		for x in (config.channels) {
			
			thread::sleep (Duration::from_millis (250));
			let s: Vec <String> = config.servers.clone();
			let f: String = config.folder.clone();
			
			handlers.push (thread::spawn (move || {
				
				let mut r: Option <Response> = None;
				for i in 0..s.len() {
					
					let awa = curl (&s [i], &x.id);
					if (awa.is_err()) {
						
						eprintln! ("{}", awa.err().unwrap());
						continue;
					}
					
					r = Some (awa.unwrap());
				}
				
				if (r.is_none()) {
					
					return;
				}
				
				if (write (&r.unwrap(), f).is_err()) {
					
					panic! ("unable to write to disk!");
				}
				
				if (! args.quiet) {
					
					eprintln! ("downloaded {}", x.name);
				}
			}));
		}
		
		for x in handlers {
			
			x.join().unwrap();
		}
		
		delete_old_responses (&config.folder).unwrap();
	}
	
	let paths: ReadDir = match fs::read_dir (&config.folder) {
		
		Ok (p) => { p },
		Err (_) => { panic! ("unable to read folder") },
	};
	
	let mut responses: Vec <Response> = Vec::new();
	
	for p in paths {
		
		let entry: DirEntry = match p {
			
			Ok (e) => { e },
			Err (_) => { panic! ("unable to read file {:?}", p) },
		};
		
		match read_json (entry) {
			
			Ok (json) => { responses.push (json); },
			Err (_) => { panic! ("unable to parse json file") },
		}
	}
	
	let mut streams: Vec <Stream> = Vec::new();
	
	for r in responses {
		
		for s in parse_json (r) {
			
			streams.push (s);
		}
	}
	
	streams.sort_by_key (|k| k.timestamp);
	table_print (streams_to_table (streams));
}

fn format_url (server: &str, channel_id: &str) -> String {
	
	return (format! ("{server}/api/v1/channels/{channel_id}/streams"));
}

fn curl (server: &str, channel_id: &str) -> Result <Response, Error> {
	
	let r: String = match reqwest::get (format_url (server, channel_id)) {
		
		Ok (ok) => {
			match ok.text() {
				Ok (ok) => { ok },
				Err (e) => { return Err (Error::from (e)) },
			}
		},
		Err (e) => { return (Err (Error::from (e))) },
	};
	
	let j: Response = match serde_json::from_str (&r) {
		Ok (ok) => {ok},
		Err (e) => { return (Err (Error::from (e))) },
	};
	
	return (Ok (j));
}

fn write (response: &Response, directory: String) -> Result <(), IOError> {
	
	let folder = Path::new (&directory);
	if (folder.is_file()) {
		
		panic! ("file exists where folder should go");
	} else if (!folder.is_dir()) {
		
		fs::create_dir (&directory)?
	}
	
	let mut timeStamp: u64 = 0;
	let s: String;
	let mut filePath: Option<&Path> = None;
	if (response.videos.len() > 0) {
		
		timeStamp = match SystemTime::now().duration_since (UNIX_EPOCH) {
			Ok (ok) => {ok.as_secs()},
			Err (_) => {0},
		};
		s = format! ("{dir}/{author}+{time}",
			dir = directory,
			author = response.videos [0].authorId,
			time = timeStamp
		);
		
		filePath = Some (Path::new (&s));
	}
	
	let f: &Path;
	if (filePath.is_none()) {
		
		panic! ("there are no videos");
	}
	
	f = filePath.unwrap();
	
	if (f.is_file()) {
		
		if (timeStamp != 0) {
			
			panic! ("file already exists")
		} else {
			
			fs::remove_file (f)?;
		}
	}
	
	let mut file: File = fs::File::create (f)?;
	
	let data: Vec <u8> = match serde_json::to_vec (&response) {
		Ok (ok) => {ok},
		Err (_) => {panic! ("I can only write so many comprehensive error messages, idfk what went wrong")},
	};
	file.write (&data)?;
	
	return Ok(());
}

fn delete_old_responses (directory: &str) -> Result <(), IOError> {
	
	let mut files: Vec <String> = Vec::new();
	for x in fs::read_dir (directory)? {
		
		let s = &x?.path().display().to_string() [directory.len() + 1..];
		files.push (s.to_string());
	}
	files.sort();
	
	let mut tmp: String = "".to_string();
	let mut file1;
	let mut file2;
	for file_name in files {
		
		if (tmp == "".to_string()) {
			
			tmp = file_name;
			continue;
		}
		
		file1 = tmp.split ("+");
		file2 = file_name.split ("+");
		if (file1.next().unwrap() == file2.next().unwrap()) {
			
			let a: u64 = match file1.next() {
				Some (ok) => { ok.parse().unwrap() },
				None => { panic! ("value missing from file name") },
			};
			let b: u64 = match file2.next() {
				Some (ok) => { ok.parse().unwrap() },
				None => { panic! ("value missing from file name") },
			};
			
			if (a < b) {
				
				fs::remove_file (format! ("{directory}/{tmp}"))?;
			} else {
				
				fs::remove_file (format! ("{directory}/{file_name}"))?;
			}
		}
		
		tmp = file_name;
	}
	
	return Ok(());
}

fn read_json (entry: DirEntry) -> Result <Response, Error> {
	
	let json_string: &str = &fs::read_to_string (entry.path())?;
	let json_result: Result <Response, serde_json::Error> = serde_json::from_str (json_string);
	let json: Response = match json_result {
		
		Ok (j) => { j },
		Err (_) => {
			
			anyhow::bail! ("fuck");
		},
	};
	
	return Ok (json);
}

fn parse_json (response: Response) -> Vec <Stream> {
	
	let mut streams: Vec <Stream> = Vec::new();
	for v in (response.videos) {
		
		let author: String = v.author;
		let title: String = v.title;
		let timestamp: u64;
		let link: String = format! ("https://www.youtube.com/watch?v={}", v.videoId);
		
		if (v.liveNow) {
			
			timestamp = 0;
		} else if (v.premiereTimestamp.is_some()) {
			
			timestamp = v.premiereTimestamp.unwrap();
		} else if (v.lengthSeconds == 0) {
			
			timestamp = 1;
		} else {
			
			continue;
		}
		
		// TODO: check if stream too old
		
		streams.push (Stream {
			
			author: author,
			title: title,
			timestamp: timestamp,
			link: link,
		});
	}
	
	return (streams);
}

fn streams_to_table (streams: Vec <Stream> ) -> Table {
	
	let now: u64 = SystemTime::now().
		duration_since (UNIX_EPOCH).
		unwrap().
		as_secs() as u64;
	let today: DateTime <Local> = Local::now();
	
	let mut curr_stage = -1;
	let mut prev_stage = -1;
	
	let mut table: Table = Table {
		
		entries: Vec::new(),
		date_length: 0,
		author_length: 0,
		title_length: 0,
	};
	
	let mut date_length = 4;
	let mut author_length = 6;
	let mut title_length = 5;
	
	table.entries.push (Table_Entry {
		
		date: "time".to_string(),
		author: "author".to_string(),
		title: "title".to_string(),
		link: "".to_string(),
		new_segment: true,
	});
	
	let mut first: bool = true;
	
	for s in streams {
		
		let mut flag: bool = first;
		
		if (now > s.timestamp) {
			
			table.entries.push ( Table_Entry {
				
				date: "[LIVE]".to_string(),
				author: s.author.clone(),
				title: s.title.clone(),
				link: s.link,
				new_segment: flag,
			});
			
			let agc = get_physical_string_size (&s.author);
			let tgc = get_physical_string_size (&s.title);
			
			if (6 > date_length) { date_length = 6; }
			if (agc > author_length) { author_length = agc }
			if (tgc > title_length) { title_length = tgc }
			
			first = false;
			continue;
		}
		
		if (curr_stage == -1 && prev_stage == -1) {
			
			curr_stage = 0;
			first = false;
			flag = true;
		}
		
		let stream_date: DateTime <Local> =
			DateTime::<Local>::from (UNIX_EPOCH + Duration::from_secs (s.timestamp));
		let diff = stream_date - today;
		
		if (diff.num_days() > 7) {
			
			break;
		}
		
		if (diff.num_days() != curr_stage) {
			
			curr_stage = diff.num_days();
			flag = true;
		}
		
		let d = format! ("{} days, {:2}h, {:2}m",
			diff.num_days(),
			diff.num_hours() - (diff.num_days() * 24),
			diff.num_minutes() - (diff.num_hours() * 60)
		);
		
		table.entries.push (Table_Entry {
			
			date: d.clone(),
			author: s.author.clone(),
			title: s.title.clone(),
			link: s.link,
			new_segment: flag,
		});
		
		let agc = get_physical_string_size (&s.author);
		let tgc = get_physical_string_size (&s.title);
		
		if (d.len() > date_length) { date_length = d.len(); }
		if (agc > author_length) { author_length = agc }
		if (tgc > title_length) { title_length = tgc }
		
		if (prev_stage != curr_stage) {
			
			prev_stage = curr_stage;
		}
	}
	
	table.date_length = date_length;
	table.title_length = title_length;
	table.author_length = author_length;
	
	return (table);
}

fn table_print (table: Table) {
	
	let mut title: bool = true;
	
	let d = table.date_length;
	let a = table.author_length;
	let t = table.title_length;
	
	let mut line_break: String = "+".to_string();
	line_break += &"-".repeat (d + 2);
	line_break += "+";
	line_break += &"-".repeat (a + 2);
	line_break += "+";
	line_break += &"-".repeat (t + 2);
	line_break += "+";
	
	for e in table.entries {
		
		if (e.new_segment) {
			
			println! ("{}", line_break);
		}
		
		let date_size = get_physical_string_size (&e.date);
		let author_size = get_physical_string_size (&e.author);
		let title_size = get_physical_string_size (&e.title);
		
		let date_length;
		let author_length;
		let title_length;
		
		if (d < date_size) {
			
			date_length = date_size - d - get_count_of_unicode_double_size_chars (&e.date);
		} else {
			
			date_length = d - get_count_of_unicode_double_size_chars (&e.date);
		}
		
		if (a < author_size) {
			
			author_length = author_size - a - get_count_of_unicode_double_size_chars (&e.author);
		} else {
			
			author_length = a - get_count_of_unicode_double_size_chars (&e.author);
		}
		
		if (t < title_size) {
			
			title_length = title_size - t - get_count_of_unicode_double_size_chars (&e.title);
		} else {
			
			title_length = t - get_count_of_unicode_double_size_chars (&e.title);
		}
		
		if (title) {
			
			println! ("| {:^date_length$} | {:^author_length$} | {:^title_length$} |", e.date, e.author, e.title);
			title = false;
			continue;
		}
		
		println! ("| {:^date_length$} | {:author_length$} | {:title_length$} |", e.date, e.author, e.title);
	}
	
	println! ("{}", line_break);
}

fn get_physical_string_size (s: &str) -> usize {
	
	let mut size: usize = 0;
	
	for c in s.chars() {
		
		size += get_physical_size_of_unicode_char (c);
	}
	
	return (size);
}

fn get_physical_size_of_unicode_char (char: char) -> usize {
	
	if (test_if_utf_8_char_is_visually_fat (char)) {
		
		return 2;
	}
	
	return 1;
}

fn get_count_of_unicode_double_size_chars (s: &str) -> usize {
	
	let mut count: usize = 0;
	
	for c in s.chars() {
		
		if (test_if_utf_8_char_is_visually_fat (c)) {
			
			count += 1;
		}
	}
	
	return (count);
}

fn test_if_utf_8_char_is_visually_fat (char: char) -> bool {
	
	let double_unicode_blocks: [(u32, u32); 14] = [
		(0x100, 0xFFFFF),
		(0x3000, 0x303F),
		(0x3040, 0x309F),
		(0x30A0, 0x30FF),
		(0x31C0, 0x31Ef),
		(0x31F0, 0x31FF),
		(0x3200, 0x32FF),
		(0x3300, 0x33FF),
		(0x4E00, 0x9FFF),
		(0x3400, 0x4DBF),
		(0x1F600, 0x1F64F),
		(0x20000, 0x2A6DF),
		(0x2A700, 0x2B73F),
		(0x2B740, 0x2B81F),
	];
	
	for d in double_unicode_blocks {
		
		if (char as u32 >= d.0 && char as u32 <= d.1) {
			
			return true;
		}
	}
	
	return false;
}
